## Exercice 1

### 1)

#### a) List all building in Japan
#### b)
#### c)
- dbo: 
- dbr: \<http://dbpedia.org/resource/>
- dbp: \<http://dbpedia.org/property/>

### 2)

```sparql
SELECT ?b ?fc WHERE {
	?b a dbo:Building;
	dbo:location dbr:Japan;
	dbo:floorCount ?fc.
}
ORDER BY DESC(?fc)
LIMIT 20
```

### 3)

```sparql
SELECT ?b ?fc ?l WHERE {
	?b a dbo:Building;
	dbo:location dbr:Japan;
	dbo:floorCount ?fc.
	rdfs:label ?l
}
ORDER BY DESC(?fc)
LIMIT 20
```

#### a) 
The name are not in english

#### b)

```sparql
SELECT ?b ?fc ?l WHERE {
	?b a dbo:Building;
	dbo:location dbr:Japan;
	dbo:floorCount ?fc;
	rdfs:label ?l.
	FILTER (LANG(?l) = "en")
}
ORDER BY DESC(?fc)
LIMIT 20
```

### 4)
#### a)
```sparql
SELECT ?b ?fc ?l WHERE {
	?b a dbo:Building;
	dbo:location dbr:Japan;
	dbo:floorCount ?fc;
	rdfs:label ?l.
	FILTER (LANG(?l) = "en")
	FILTER (?fc > 50)
}
ORDER BY DESC(?fc)
```

#### b

```sparql
SELECT COUNT(?b) WHERE {
	?b a dbo:Building;
	dbo:location dbr:Japan;
	dbo:floorCount ?fc;
	rdfs:label ?l.
	FILTER (LANG(?l) = "en")
	FILTER (?fc > 50)
}
```
### 5)

Count buildings with fc > 50 and russian label

```sparql
SELECT COUNT(?b) WHERE {
	?b a dbo:Building;
	dbo:location dbr:Japan;
	dbo:floorCount ?fc;
	rdfs:label ?l.
	FILTER (LANG(?l) = "ru")
	FILTER (?fc > 50)
}
```


```sparql
SELECT ?b ?fc ?l WHERE {
	?b a dbo:Building;
	dbo:location dbr:Japan;
	dbo:floorCount ?fc;
	rdfs:label ?l.
	FILTER (LANG(?l) = "ru")
	FILTER (?fc > 50)
}
ORDER BY DESC(?fc)
```


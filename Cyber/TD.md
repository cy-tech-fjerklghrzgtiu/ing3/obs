![[Excel 2024-03-22 09.45.08.sheet]]

![[TD.drawio.svg]]

| Réseau  | IP          | Mask | GW            |
| ------- | ----------- | ---- | ------------- |
| MécaFam | 192.168.0.0 | 16   | 192.168.0.254 |

## TODO

- SPDF
- VLAN
- DMZ => IIS
- BACKUP
- Chiffrement portables
- VPN
	- Modelis
	- Nomade
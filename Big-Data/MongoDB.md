## Exercises
### Restaurants
https://www.w3resource.com/mongodb-exercises/#MongoDB_restaurants
1.
```js
db.restaurants.find()
```
2.
```js
db.restaurants.find({},{"restaurant_id":true,"name":true,"borough":true,"cuisine":true})
```
3.
```js
db.restaurants.find({},{"restaurant_id":true,"name":true,"borough":true,"cuisine":true,"_id":false})
```
4.
```js
db.restaurants.find({},{"restaurant_id":true,"name":true,"borough":true,"address.zipcode":true,"_id":false})
```
5.
```js
db.restaurants.find({"borough":"Bronx"})
```
6.
```js
db.restaurants.find({"borough":"Bronx"}).limit(5)
```
7.
```js
db.restaurants.find({"borough":"Bronx"}).skip(5).limit(5)
```
8.
```js
db.restaurants.find({grades: {$elemMatch:{"score":{$gt:90}}}})
```
9.
```js
db.restaurants.find({grades: {$elemMatch:{"score":{$gt:80,$lt:100}}}})
```
10.
```js
db.restaurants.find({"address.coord":{$lt:-95.754168}})
```
11.
```js
db.restaurants.find({"address.coord":{$lt:-65.754168},"grades":{$elemMatch:{"score":{$gt:90}}},"cuisine":{$ne:"American"}})
```
12.
```js
db.restaurants.find({"address.coord":{$lt:-65.754168},"grades":{$elemMatch:{"score":{$gt:90}}},"cuisine":{$ne:"American"}})
```
13.
```js
db.restaurants.find({"name": /^Wil/},{"_id":false,"restaurant_id":true, "name":true, "borough":true})
```
15.
```js
db.restaurants.find({"name": /^.+ces$/},{"_id":false,"restaurant_id":true, "name":true, "borough":true})
```
16.
```js
db.restaurants.find({"name": /^.+Reg/},{"_id":false,"restaurant_id":true, "name":true, "borough":true})
```
17.
```js
db.restaurants.find({"borough": "Bronx", $or:[{"cuisine":"American"},{"cuisine":"Chinese"}]},{"_id":false,"restaurant_id":true, "name":true, "borough":true})
```
18.
```js
db.restaurants.find({"borough": {$in: ["Bronx", "Staten Island", "Queens", "Brooklyn"]}},{"_id":false,"restaurant_id":true, "name":true, "cuisine":true, "borough": true})
```
19.
```js
db.restaurants.find({"borough": {$nin: ["Bronx", "Staten Island", "Queens", "Brooklyn"]}},{"_id":false,"restaurant_id":true, "name":true, "cuisine":true, "borough": true})
```
20.
```js
db.restaurants.find({grades: {$elemMatch:{"score":{$lt:10}}}},{"_id":false,"restaurant_id":true, "name":true, "cuisine":true})
```
51.
```js
db.restaurants.aggregate([{
    $unwind: "$grades"
  },
  {
    $group: {
      _id: "$name",
      avgScore: {
        $avg: "$grades.score"
      }
    }
  }
])
```
52.
```js
db.restaurants.aggregate([{
    $unwind: "$grades"
  },
  {
    $group: {
      _id: "$name",
      highest_score: {
        $max: "$grades.score"
      }
    }
  }
])
```
53.
```js
db.restaurants.aggregate([{
    $unwind: "$grades"
  },
  {
    $group: {
      _id: "$name",
      highest_score: {
        $min: "$grades.score"
      }
    }
  }
])
```
54.
```js
db.restaurants.aggregate([
  {
    $group: {
      _id: "$borough",
      num: {
        $sum: 1
      }
    }
  }
])
```
55.
```js
db.restaurants.aggregate([
  {
    $group: {
      _id: "$borough",
      num: {
        $sum: 1
      }
    }
  }
])
```
56.
```js
db.restaurants.aggregate([
  {
    $group: {
      _id: {"cuisine": "$cuisine", "borough":"$borough"},
      num: {
        $sum: 1
      }
    }
  }
])
```
57.
```js
db.restaurants.aggregate([{
    $unwind: "$grades"
  },
  {
	$match: {"grades.grade":"A"}
  },
  {
    $group: {
      _id: {"cuisine": "$cuisine"},
      num: {
        $sum: 1
      }
    }
  }
])
```
58.
```js
db.restaurants.aggregate([{
    $unwind: "$grades"
  },
  {
	$match: {"grades.grade":"A"}
  },
  {
    $group: {
      _id: {"borough": "$borough"},
      num: {
        $sum: 1
      }
    }
  }
])
```
59.
```js
db.restaurants.aggregate([{
    $unwind: "$grades"
  },
  {
	$match: {"grades.grade":"A"}
  },
  {
    $group: {
      _id: {"borough": "$borough","cuisine": "$cuisine"},
      num: {
        $sum: 1
      }
    }
  }
])
```
61.
```js
db.restaurants.aggregate([{
    $unwind: "$grades"
  },
  {
    $group: {
      _id: {"cuisine": "$cuisine"},
      avg: {
        $avg: "$grades.score"
      }
    }
  }
])
```
62.
```js
db.restaurants.aggregate([{
    $unwind: "$grades"
  },
  {
    $group: {
      _id: {"cuisine": "$cuisine"},
      max: {
        $max: "$grades.score"
      }
    }
  }
])
```
63.
```js
db.restaurants.aggregate([{
    $unwind: "$grades"
  },
  {
    $group: {
      _id: {"cuisine": "$cuisine"},
      min: {
        $min: "$grades.score"
      }
    }
  }
])
```
64.
```js
db.restaurants.aggregate([{
    $unwind: "$grades"
  },
  {
    $group: {
      _id: {"borough": "$borough"},
      avg: {
        $avg: "$grades.score"
      }
    }
  }
])
```
65.
```js
db.restaurants.aggregate([{
    $unwind: "$grades"
  },
  {
    $group: {
      _id: {"borough": "$borough"},
      max: {
        $max: "$grades.score"
      }
    }
  }
])
```
66.
```js
db.restaurants.aggregate([{
    $unwind: "$grades"
  },
  {
    $group: {
      _id: {"borough": "$borough"},
      min: {
        $min: "$grades.score"
      }
    }
  }
])
```
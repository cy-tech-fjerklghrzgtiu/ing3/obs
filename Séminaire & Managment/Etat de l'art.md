
## Situation marquante dans une petite équipe IT d'une entreprise du secteur des ERP

**I. Présentation de l'organisation**

**A. Contexte organisationnel**

Je suis actuellement en stage au sein de la Direction des Systèmes d'Information (DSI) d'une grande entreprise du secteur des ERP. La DSI est composée de plusieurs départements, dont un dédié au développement et à la maintenance des progiciels de gestion intégrés (ERP). C'est dans ce département que j'ai été intégré au sein d'une petite équipe de 5 développeurs.

**B. Division du travail et centralisation du pouvoir**

La DSI est caractérisée par une forte division du travail. Chaque équipe est spécialisée dans un module métier spécifique de l'ERP (finance, comptabilité, production, etc.). Le pouvoir est relativement centralisé au sein de la DSI, avec un Directeur des Systèmes d'Information qui détermine les orientations stratégiques et les priorités des projets.

**C. Buts et missions**

Le but principal de la DSI est de fournir aux différents métiers de l'entreprise les outils et les services informatiques nécessaires à leur activité. Les missions de la DSI incluent le développement et la maintenance des progiciels de gestion intégrés, la gestion de l'infrastructure informatique, la sécurité des données et le support aux utilisateurs.

**II. Présentation de l'équipe**

**A. Missions principales**

L'équipe dans laquelle j'ai été intégré est responsable du développement et de la maintenance d'un module métier de l'ERP, en l'occurrence le module de gestion de la production.

**B. Membres de l'équipe**

L'équipe est composée de 5 développeurs :

- **Chef d'équipe:** 35 ans, homme, 10 ans d'ancienneté dans l'entreprise, formation ingénieur informatique.
- **Développeur senior:** 30 ans, femme, 5 ans d'ancienneté dans l'entreprise, formation ingénieur informatique.
- **Développeur junior:** 25 ans, homme, 2 ans d'ancienneté dans l'entreprise, formation Master informatique.
- **Deux stagiaires:** 22 ans, hommes, en dernière année d'école d'ingénieur.

**III. Description du manager**

**A. Profil du manager**

Le chef d'équipe est un homme de 35 ans, ingénieur informatique de formation, avec 10 ans d'ancienneté dans l'entreprise. Il est décrit par ses collaborateurs comme étant un excellent technicien, doté d'une grande expertise dans son domaine. Il est également reconnu pour ses qualités humaines et sa capacité à fédérer son équipe.

**B. Missions et fonctions**

Le chef d'équipe est responsable de la planification des projets, de l'allocation des tâches aux membres de l'équipe, du suivi de l'avancement des projets et de la résolution des problèmes techniques. Il est également le garant de la qualité du code et de la satisfaction des clients.

**C. Posture managériale**

Le chef d'équipe adopte une posture managériale situationnelle. Il adapte son style de management en fonction du contexte et des besoins de ses collaborateurs. Il peut ainsi être directif lorsqu'il est nécessaire de prendre des décisions rapides, ou plus délégatif lorsqu'il souhaite encourager l'autonomie de ses collaborateurs.

**D. Analyse de la posture managériale**

Le chef d'équipe est capable d'adapter son style de management aux différentes situations. Cette flexibilité est particulièrement importante dans le secteur des ERP, où les projets sont souvent complexes et les clients exigeants.

**IV. Description de la situation**

**A. Contexte**

L'équipe était en charge d'un projet de développement majeur pour un client important. Le projet consistait à implémenter un nouveau module de gestion de la production dans l'ERP du client.

**B. Enjeux**

Les enjeux de ce projet étaient importants pour l'entreprise. Le succès du projet était crucial pour la satisfaction du client et la fidélisation de son business.

**C. Protagonistes**

Les protagonistes de cette situation étaient le chef d'équipe, les membres de l'équipe et le client.

**V. Analyse de la situation**

**A. Stratégie du manager**

Le chef d'équipe a mis en place une stratégie de management situationnelle. Il a d'abord défini un plan de projet clair et précis. Ensuite, il a


# Forterro

### Présentation de l'organisation

Lors de la situation de management que je souhaite décrire, j'étais en stage dans le département IT Infrastructure du groupe Forterro, un regroupement d'éditeurs de solution ERP (Enterprise resource planning).

Le but systèmes de ce groupe et de devenir un des leaders du marché européen des ERP de petites et moyenne entreprises en fournissant un large éventails de solution adapté à chaque secteur par le biais des différentes entités du groupe.

Le regroupement des nombreuses entreprises faisant maintenant partie de Forterro à aussi pour but de rationalisé les coups d'infrastructure IT et administratifs est de simplifier l'administration des ressources afin de garantir la qualité des produits.

Dans le département dans lequel j'ai effectué mon stage, le pouvoir de décision final était détenu par le chef de service, cependant chaque membre de celui-ci était libre de proposer des nouveaux projets/amélioration et de s'attribuer du temps pour les essayer/mettre en œuvre.

C'est donc avec l'idée d’amélioré un service interne que j'ai était recruté par un des architectes IT, afin de réaliser cette mission en autonomie.

### Présentation de l'équipe

L'équipe que j'ai intégré est donc celle des Architectes IT, leur rôle est de fournir et maintenir une plateforme informatique afin déployer les applications internes de l'entreprise ainsi que les produits développés par les différentes BU du groupe de manière fiable, simple et peu coûteuse. Celle-ci est composée de huit membres principaux s'occupant des composants les plus critiques de l'infrastructure.

Dans le cadre de mon stage, j'étais directement en relation avec deux autres Architectes IT de mon équipe afin qu'ils puissent suivre l'avancement général de ma mission. Ainsi que quelques utilisateurs d'un des services internes sur lequel, je devais intégrer une nouvelle fonctionnalité afin de pouvoir affiner les besoins. 

J'étais le membre le plus jeune de l'équipe dans laquelle j'ai été intégré, avec mes deux collègues étant frère et la plupart de mes collègues travaillaient ensemble depuis de nombreuses années dans un autre entreprise avant d'arriver chez Forterro. De ce fait, l'équipe que j'ai rejoint disposait d'une forte cohésion, et j'ai été bien accueilli.

Cependant, il était difficile de définir une vraie hiérarchie entre les membres de l'équipe et les échanges entre eux était très organiques. Tous les process n'étaient pas forcément documentés et il était parfois long de trouver une information sans avoir à déranger mes collègues qui sont souvent très occupés.

### Description du manager

Olivier Gintrand Mon manager lors de ce projet était le deuxième le plus ancien du service, il a été le principal responsable de l'architecture actuelle des infrastructures de production. Avant d'être employé chez Forterro, il était Architecte réseau chez Thalès et avant ça dans l'armée de terre. 

Ayant beaucoup d'expérience concrète dans son domaine, il est celui qui mène généralement les gros projets et suis aussi les projets plus petits menés par les différents membres de son équipe. C'est quelqu'un de très amical et qui donne pas mal de liberté à ces subordonnés tout en gardant un œil sur l’avancement de ceux-ci. 

Il respecte la diversité d'opinion, mais fini souvent par choisir ces propres idées lorsqu'il s'agit d'un sujet qu'il a déjà connu et qu'il ne voit pas d'avantage clair aux autres solutions. 

Afin de suivre les différents projets, la méthode Scrum est utilisé. Chaque jour un meeting est organisé pour chaque projet et chaque membre décrit brièvement son avancement, ces difficultés et les possibles contraintes qui sont apparues, chaque personne n'a que quelques minutes pour parler pour ne pas empiéter sur le temps de travail de chacun. Chaque mois, une réunion plus longue est planifié afin de faire le point sur l'avancement général des projets.

Si l'un de nous a besoin d'information ou d'aide, il ne doit pas hésiter à contacter d'autres membres de l'équipe afin de ne pas rester bloqué. L'entraide et la communication sont fortement encouragées.

### Description et analyse de la situation

Je vais donc présenter ma situation lors du projet principal que j'ai dû réaliser lors de ma période de stage. 

Avant de commencer ma mission, j'ai d'abord été contacté par Olivier afin qu'il me présente en détail le but et les objectifs majeurs de ma future mission. Il m'a remis en contexte les attentes de ce projet et en quoi il était essentiel pour l'évolution de l'entreprise.

Une fois arrivé dans l'entreprise, mon manager m'a d'abord présenté à l'équipe et expliqué les différents outils dont j'aurais besoin lors de cette mission. Par la suite, j'ai eu un debrief avec les personnes directement concerné par l'outil que je devrais créer afin de comprendre le besoin précis.

Une fois le projet commencé, j'ai eu de bref réunions quotidiennes avec mon manager et de temps en temps avec d'autres personnes concerné par le projet, afin encore une fois de pouvoir s'assurer que le projet avance bien et qu'il soit conforme aux attentes du "client".

À la fin de projet, j'ai eu une réunion avec toutes les personnes concernée par le projet afin de pouvoir présenter le produit final, son intérêt au sein de l'organisation ainsi que ses possibles évolutions.

### Analyse de la situation

Olivier Gintrand adaptait son style de leadership en fonction de la situation et des besoins de ses collaborateurs. Il pouvait être directif lorsqu'il le fallait, mais il n'hésitait pas à déléguer et à faire confiance à ses équipes. Cette flexibilité a permis de créer un climat de confiance et de responsabilisation au sein du service.

Avant de commencer le projet, Olivier Gintrand a pris le temps de me présenter en détail les objectifs à atteindre. Il m'a également donné le contexte nécessaire pour comprendre l'importance de ce projet pour l'entreprise. Cette clarté de vision a été un élément essentiel du succès du projet.

Des réunions quotidiennes et des points d'étape réguliers étaient organisés pour suivre l'avancement du projet et identifier les potentiels problèmes. Olivier Gintrand était toujours disponible pour apporter son soutien et proposer des solutions. Sa réactivité et sa capacité à prendre des décisions rapides ont permis de surmonter les obstacles rencontrés et de maintenir le cap.

Malgré les nombreux points forts de la stratégie de management d'Olivier Gintrand, il y a quelques points d'amélioration à explorer. La centralisation du pouvoir peut parfois freiner la prise de décision et limiter l'autonomie des collaborateurs. De plus, un manque de documentation et un accès difficile à l'information peuvent ralentir le travail et créer des frustrations.

L'expérience que j'ai vécue au sein du groupe Forterro a été très enrichissante. J'ai pu observer et apprendre d'un manager expérimenté qui a su créer un environnement de travail positif et propice à la réussite. La stratégie de management d'Olivier Gintrand est basée sur la confiance, la collaboration et la responsabilisation. Elle s'est avérée efficace dans le cadre de ce projet, mais il y a quelques points d'amélioration à explorer pour maximiser le potentiel des équipes.
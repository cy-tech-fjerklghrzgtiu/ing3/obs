## Forterro

### Buts

- Innover
- Dominer le marché européen
- Gagner Plus

### Division du travail

- Hybride
	- Division du travail assez flou (conception et réalisation faites par une personne/équipe)
	- Travaille assez diversifié

### Distribution du pouvoir

- Stratégie d'ensemble centralisée et définie par les propriétaires
- Réalisation et organisation des équipes encore décentralisée et flexible

### Mécanismes de coordination

- Ajustement mutuel
- Tend vers la standardisation des procédés et normes
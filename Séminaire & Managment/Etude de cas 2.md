
### 1

- Réunion avec les deux collaborateurs
- Définir une hiérarchie plus précise

### 2

1. Description de l'objectif
2. Besoin de moderniser le service
	- Chef actuel Expérimenté
	- Mais manque de connaissances sur les nouveaux objectifs
3. Introduction d'une employer avec des connaissances plus adaptées
	- Peut aider à moderniser
	- Connais peu le service
	- Souhaite faire ses preuves
4. Mme Legrand à une volonté de remplacer le chef actuel par Mlle Martin
5. Informe le chef actuel du changement
	- Manque de transparence envers le chef actuel
6. Le travail est démarré, mais avance peu
7. Mme Legrand fait un point avec Mlle Martin puis Mr Chevalier
	- Chacun reproche un manque communication
	- On découvre qu'ils n'ont pas collaborés
	- Mr Chevalier serait trop rigide
	- Mlle Martin serait trop exigeante avec les employés du service

La situation bascule, Mme Legrand demande un point aux deux collaborateurs

### 3

#### Mr Chevalier

- But
	- Ne pas se brusquer
	- Continuer comme avant
	- Faire ce qu'il sait faire
	- Finir sa carrière tranquillement
- Enjeux
	- Attendre la retraite
	- Garder son image dans l'équipe
- Sources de Pouvoirs
	- Ancienneté 
	- Expérience
	- Reconnaissance de ses pairs
- Contraintes
	- Réfractaire à l'outil informatique
	- Fermé à l'innovation
- Stratégies
	- se laissé porter
	- garder sa place
#### Mlle Martin

- But
	- évoluer dans l'entreprise
	- Informatiser le service
	- Faire ses preuves
- Enjeux
	- Sa future position/carrière
	- Crédibilité
- Sources de Pouvoirs
	- Expertise sur le projet de modernisation
	- Mme Legrand
- Contraintes
	- S'adapter à une nouvelle équipe
	- Mr Chevalier qui ne veut pas changer sa manière de faire
		- peu coopératif
- Stratégies
	- Prendre en main le projet
	- Rentrer en contact avec l'équipe
	- Passer outre Mr Chevalier
#### Mme Legrand

- But
	- Moderniser le service efficacement
	- Trouver un remplaçant pour Mr Chevalier
- Enjeux
	- Efficacité du service
	- 
- Sources de Pouvoirs
	- Statut Hiérarchique
- Contraintes
	- Mettre en relation deux personnes avec des idées différentes
	- 
- Stratégies
	- Donner du pouvoir à Mlle Martin
	- Faire des points régulier

#### 4

- Faire comprendre aux deux parties qu'ils doivent faire des efforts pour accepter la manière de travailler de l'autre
- Mettre en avant les compétences de chacun
- Affiner leurs rôles dans la stratégie de modernisation
# Démarche & Analyse 

## Besoin

- Cible
	- __étudiants__
	- lycéens
	- potentiels, partenaires
	- __recruteurs__
- Identité
	- __Propre à chaque option__
	- __Unité au sein de l'université__
	- Ancienne école privé devenu publique
	- Fusion entre privé et public
- Objectif
	- Montré la croissance
- Social
	- Ouverture culturel
	- Adapté à chacun
- Thèmes
	- Recherche
	- __Professionnel__ 
	- Durable
	- __Avenir__
	- Transition vers nouvelle société

##### Besoin/Idées des options

- HPDA
	- Beaucoup de donnée
	- Performance
	- Math & Info
	- Parallélisme
- ICC
	- Architecture logicielle et système
	- Information
	- Décentralisé
	- Unification
	- Disponibilité
- IA
	- Optimisation
	- Deep Learning/NLP
	- GPU
	- Big Data
- CS
	- Sécurité
	- Système
	- DIY
	- Réseaux

### Besoin graphique

- Sobre
- __harmonieuses__
- __Lisible et simple__
- Adapté aux handicaps (daltoniens, relief)

- Déclinaisons
	- __Numérique__
		- Couleurs RGB
		- Favicon
		- Signature Mail
		- Header
	- __Monochrome__
	- Papier
		- Couleurs CMYB 
		- Simplifier
		- Affiche
	- Physique
		- Relief

## Analyse

### CY-Tech

- Séparation entre les options
	- Par Typo
	- Par Couleur
		- __Accent__
	- Par Formes
		- __éléments__
- Unicité au sein de l'université
	- __Par Typo__
	- Par Couleur
		- __Palette__
	- Par Forme
		- __Composition__

Chaque logo aura une palette de couleur commune, mais une couleur d'accentuation unique représentant les valeurs et inspirations de chaque option.

La typographie (Police) sera identique afin d'unifier et de facilité la lecture des logos.


Chaque option aura une couleur et des formes propres a celle-ci affin de la distinguer.

#### Composition

La Composition sera similaire. Et servira à représenter les idéologies générales de CY-Tech. Chaque option pourra prendre des libertés vis-à-vis de celle-ci sans trop s'en éloigner 

La composition devra ainsi représenter l'une ou plusieurs de ces notions :

- Professionnalisme
	- Structuré
- Ouverture/Accessibilité
- Avenir
	- de haut en bas => comme un bâtiment, il est construit et s'étant vers de plus hauts sommets
- L'unité
	- L'ensemble des logos devront symboliser un même thème
	- Formes communes des cercles qui rappellent les formes du logo CY-Tech

Les logos sortiront d'une ligne horizontale pouvant représenter le sol duquel sort l'option pour montée vers le ciel, indiquant la croissance et le développement.

__IA__
- Symbole de l'infini pour montrer des possibilités infinies et l'avenir du domaine
- 
#### Couleurs

- Couleurs CY-Tech
### Options

